﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointFollower : MonoBehaviour 
{
    [SerializeField] float speed = 4f;
    [SerializeField] float angularSpeed = 0.8f;
    [SerializeField] Transform[] waypoints;
    [Tooltip("Distance from waypoint before selecting next waypoint")][SerializeField] float distanceThreshold = 0.4f; 

    int currentIndex = 0;


   // Use this for initialization
	
   void Start () 
   {

		
	
   }

   // Update is called once per frame
   void LateUpdate () 
   {
        // get waypoint position
        Vector3 wp = GetWaypointPosition();

        // move at speed toward waypoint
        Vector3 direction = wp - transform.position;

        // normalize the y position to that of the transform
        direction.y = transform.position.y;

        transform.position = transform.position + (direction.normalized * speed * Time.deltaTime);

        // need to rotate into the same vector as the way point (face to the waypoint)
        float step = angularSpeed * Time.deltaTime;
        Vector3 newDir = Vector3.RotateTowards(transform.forward, direction, step, 0.0F);
        Debug.DrawRay(transform.position, newDir, Color.red);
        transform.rotation = Quaternion.LookRotation(newDir);
    }

    private Vector3 GetWaypointPosition()
    {
        Transform wp = waypoints[currentIndex];

        Vector3 waypointLocation = wp.position;
        waypointLocation.y = transform.position.y; // keep vector in current object's XZ plane.

        // calculate distance to current waypoint
        float dist = Vector3.Distance(transform.position, waypointLocation);

        // if distance is within threshold value, select next waypoint
        if (dist <= distanceThreshold)
        {
            currentIndex++;
            if (currentIndex >= waypoints.Length)
            {
                currentIndex = 0;
            }

            wp = waypoints[currentIndex];
        }

        // return waypoint selected
        return wp.position;
    }

}
